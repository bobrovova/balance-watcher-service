/**
 * Base class For Providers (REST, JSON etc)
 */
class Provider {
    constructor(){}

    /**
     * Should be redefined in inheritor class
     */
    getWallets(){};

    updateWallet(){};
}

module.exports = Provider;
