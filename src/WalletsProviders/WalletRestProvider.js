const request = require('request-promise');
const Provider = require('./WalletsProvider');

class WalletsRestProvider extends Provider {
    constructor(){
        super();
    }

    getWallets(callback){
        return request('http://localhost:7777/addresses/');
    }

    updateWallet(address, data, callback){
        return request.put({
            url: 'http://localhost:7777/addresses/' + address,
            form: data
        });
    }
}

const walletsRestProviderSinglton = new WalletsRestProvider();

module.exports = walletsRestProviderSinglton;
