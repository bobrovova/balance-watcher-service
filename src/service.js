const hydra = require('./hydra');
const walletsProvider = require('./WalletsProviders/WalletRestProvider');
const Web3 = new require('web3');
const Promise = require('bluebird');
const Schedule = require('node-schedule');
const BigNumber = require('big-number');

let web3;

if (typeof web3 !== 'undefined') {
  web3 = new Web3(web3.currentProvider);
} else {
    // set the provider you want from Web3.providers
  web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
}

class BalanceWatcherService {
    constructor(){
        this.hydraReady = false;
        this.addresses = [];
    }

    ready(){
        return hydra.ready();
    }

    checkBalance(address, addressValue){
        if(this.addresses[address] !== undefined){
            this.addresses[address].balance = BigNumber(this.addresses[address].balance)
                .plus(addressValue)
                .toString();
            walletsProvider.updateWallet(
                address,
                {
                    balance: this.addresses[address].balance

                }
            );
            return true;
        }
        return false
    }

    watchTransactions(){
        hydra.on('message', (function(message){
            if(message.typ == 'transactions'){
                let transactions = message.bdy.data;
                let signal = false;
                transactions.forEach(function(item, i, arr){
                    if(item.value != 0) {
                        signal = this.checkBalance(item.from, -item.value);
                        signal = signal || this.checkBalance(item.to, item.value);
                    }
                }, this);

                if(signal == true){
                    let message = hydra.createUMFMessage({
                        to: 'admin-update-service',
                        from: 'transactions-watcher-service',
                        body: {}
                    });
                    message.type = 'update-balance';
                    hydra.sendMessage(message);
                }
            }
        }).bind(this));
    }

    watchUpdateAddresses(){
        hydra.on('message', function(message){
            console.log('Update addresses');
        });
    }

    start(){
        Promise.all([
            walletsProvider.getWallets(),
            balanceWatcherService.ready()
        ])
            .spread((function(walletsResponse, hydraReady){
                let addresses = JSON.parse(walletsResponse);
                addresses.forEach(function(item, i, arr){
                    //console.log(item);
                    this.addresses[item.address] = {
                        balance: item.balance
                    }
                }, this);
                //console.log(this.addresses);
                hydra.registerService();
            }).bind(this))
            .then((this.watchTransactions).bind(this))
            .then(this.watchUpdateAddresses);
    }

    isReady(){
        return this.hydraReady;
    }
}

let balanceWatcherService = new BalanceWatcherService();
balanceWatcherService.start();

module.exports = BalanceWatcherService;
